<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creating a new user
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $user = new User;
        $user->name = 'Ismoil Shifoev';
        $user->email = 'alex.malikov94@gmail.com';
        $user->password = Hash::make('ismoil1234');
        $user->save();
        $user->roles()->attach($role_user);

        $admin = new User;
        $admin->name = 'Leo Messi';
        $admin->email = 'leomessi@gmail.com';
        $admin->password = Hash::make('leo1234');
        $admin->save();
        $admin->roles()->attach($role_admin);

    }
}
