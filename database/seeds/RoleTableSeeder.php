<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creating a new role for users
        $role_user = new Role();
        $role_user->name = 'user';
        $role_user->description = 'A regular user';
        $role_user->save();

        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'An admin role';
        $role_admin->save();

    }
}
