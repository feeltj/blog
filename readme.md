## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

## INSTALLING PHP

An equivalent for Windows users could be to download and install **XAMPP**. **XAMPP** comes with a UI for installing most of the other things you have to install manually below. Hence, Windows users may skip the next few steps until the Installing Composer sub-heading.

## Cloning Project
```
$ git clone https://feeltj@bitbucket.org/feeltj/blog.git

```

## Installing Composer 

When you clone the project go to telminal and run commands

```
$ cd blog
$ composer install
$ php artisan serve

```

## Database configuration

Create a new MySQL database and call it `blog` in **XAMPP** or you can do it on production. In the  `.env` file, update the database configuration keys as seen below:
```
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=blog
    DB_USERNAME=YourUsername
    DB_PASSWORD=YourPassword

```


 Now let’s test the application to see if it is working as intended. Before running the application, we need to refresh our database so as to revert any changes. To do this, run the command below in your terminal:

`$ php artisan migrate:fresh --seed`

 Next, let’s build the application so that all the changes will be compiled and included as a part of the JavaScript file. To do this, run the following command on your terminal:

`$ npm run dev`

 Finally, let’s serve the application using this command:

`$ php artisan serve`

 To test that our application works visit the application URL **http://localhost:8000** on two separate browser windows, we will log in to our application on each of the windows as a different user.

 ** Custom User for login **
```
 Username: alex.malikov94@gmail.com
 Password: ismoil1234
```

** Custom User for Admin Panel **

```
 Username: leomessi@gmail.com
 Password: leo1234
```
